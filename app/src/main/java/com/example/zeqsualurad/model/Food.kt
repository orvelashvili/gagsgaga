package com.example.zeqsualurad.model

data class Food (
    val id: String? = null,
    val title: String? = null,
    val description: String? = null,
    val imageUrl: String? = null
)